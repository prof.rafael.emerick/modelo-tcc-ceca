\chapter{Desenvolvimento}
\thispagestyle{plain}
\label{cap:desenvolvimento}
\graphicspath{{./Cap3_Desenvolvimento/Figures/}}

Para o presente trabalho, foi definido um conjunto de sete gestos a serem reconhecidos, que serão mais bem ilustrados na Seção \ref{sec:class_gesto}. A ferramenta de avaliação matemática do método proposto a ser utilizada é o \textit{software} Matlab, portanto, todas as funções referidas foram implementadas em linguagem própria deste \textit{software}, sendo que algumas destas funções fazem parte do conjunto de ferramentas disponível neste \textit{software}. Dentre os vários métodos disponíveis na literatura, para este trabalho foi adotada uma variação do método descrito em \citeonline{chen2014}, sendo a detecção dos gestos feita pela segmentação dos dedos da imagem. Cada dedo é representado pelo vetor formado entre seu centro de massa e o centro da palma da mão, e cada gesto é formado por cinco vetores, que representam os possíveis dedos. Esta composição de entrada é então passada à um classificador, que determina qual é o gesto formado. Foram utilizados três tipos diferentes de classificadores, o primeiro é um classificador que utiliza de regras comuns para a aferição de um gesto, o segundo tem por base uma rede neural artificial e o último uma máquina de aprendizagem do tipo SVM. A Figura \ref{fig:fluxograma_processamento} esquematiza as etapas do processo para o reconhecimento de um gesto, no qual a imagem adquirida tem seu fundo removido e, a partir do centro da palma da mão, são traçados vetores que representam o gesto realizado. O conjunto de vetores é então passado a um classificador que determina qual o gesto mais provável.

\begin{figure}[h]
    \centering
    \caption{Fluxograma de Processamento.}
    \includegraphics[width=\textwidth]{fluxograma_processamento.pdf}
    \caption*{Fonte: O autor.}
    \label{fig:fluxograma_processamento}
\end{figure}

Nas seções a seguir, serão detalhadas cada uma das etapas descritas no fluxograma, começando pelo processo separação da mão do restante da imagem, passando à identificação e remoção da palma da mão, formação dos vetores que representam um gesto e, por fim, o processo de classificação deste gesto.

\section{Delimitação da região da mão}\label{sec:reg_mao}
Sendo o primeiro procedimento realizado, a delimitação da região que representa a mão, cujo gesto deve ser identificado, ocorre pela detecção do maior objeto presente na imagem que possua coloração de pele humana. Para minimizar efeitos de iluminação, é aplicado um algoritmo de balanço de branco do tipo \textit{gray world} \mbox{\cite{color_balancing_algorithms:2017}}. A imagem, com o balanço de branco corrigido, é então convertida do espaço RGB para o YC$_b$C$_r$. Neste novo espaço, a detecção é feita por um limiar simples, onde um pixel é tido como 'cor de pele' quando a componente Cb está entre 77 e 135, e sua componente Cr entre 135 e 173. Este valor foi escolhido com base em experimentos realizados com as amostras colhidas, sendo capaz de detectar corretamente a mão em um conjunto de 1400 amostras. Ao fim deste procedimento, é gerada uma imagem binária, onde 1 representa um pixel com ocorrência positiva para "cor de pele" e 0 o caso contrário. Pode-se então definir a função
%
\begin{equation}
    F_{sombra}(X,Y) =
    \begin{cases}
      1, & \text{se}\ (77 <Cb< 135)\ \text{e}\  (135<Cr<173)\\
      0, & \text{caso contrário}
    \end{cases}   
    \label{eq:f_sombra}
\end{equation}
%
como a função que determina a região onde há cor de pele, sendo $X$ a coordenada horizontal e $Y$ a coordenada vertical do plano no qual se situa a imagem.

Após a identificação das possíveis regiões com objetos com cor de pele humana, é feita a seleção do maior objeto com esta característica na figura, com o auxílio da função \textit{bwareafilt} \cite{bwareafilt_doc:2017}. Desta forma, a mão dever ser, obrigatoriamente, o maior objeto com cor de pele presente na amostra. A Figura \ref{fig:remocao_fundo} ilustra este processo, onde o resultado é uma imagem binária com a "sombra" da mão.

\begin{figure}[h]
    \centering
    \caption[Identificação da sombra da mão]{Identificação da sombra da mão.}
    \includegraphics[width=\textwidth]{deteccao_mao.pdf}
    \caption*{Fonte: O autor.}
    \label{fig:remocao_fundo}
\end{figure}

\section{Identificação e remoção da palma da mão}
Tendo a região da figura a ser trabalhada sido definida, é preciso, então, definir quais serão as grandezas mensuradas nesta imagem para que seja feita a identificação de um determinado padrão, ou gesto. O primeiro ponto significativo será o centro da palma da mão, pois é a partir deste que será traçada uma estratégia para a remoção da região da palma da mão \cite{chen2014}, o que permitirá identificar cada dedo como um objeto separado. Isso será fundamental para a construção dos vetores que serão, em última instância, a representação simplificada do gesto proferido.

O ponto do centro da palma da mão é definido como sendo o ponto mais distante de qualquer borda da figura da mão. Para a sua detecção é utilizada a transformada de distância, implementada pela função \textit{bwdist} \cite{distance_transform_doc:2017}, que calcula, para cada pixel da imagem, a mínima distância até uma borda. A imagem gerada pela transformada de distância é também chamada de esqueleto da imagem \cite{velho:2000} e está ilustrada na Figura \ref{fig:esqueleto}. Quanto maior a distância de um pixel até a borda, mais claro ele é tingido, assim, nesta figura, o ponto mais próximo do branco é o que representa o centro da palma da mão.

\begin{figure}[h]
    \centering
    \caption{Transformada de distância aplicada a sombra da mão.}
    \includegraphics{esqueleto.pdf}
    \caption*{Fonte: O autor.}
    \label{fig:esqueleto}
\end{figure}

É importante ressaltar que, em alguns casos, o processo de detecção, da etapa anterior, gera imagens com 'buracos', ou seja, devido a alguma inconformidade na exposição, surgem alguns pixeis dentro da região da mão que não foram identificados como 'cor de pele', fazendo com que a detecção do centro da palma da mão fique comprometida. Devido a este fato, foi aplicado um método de preenchimento, aqui fornecido pela função \textit{imfill} \mbox{\cite{imfill_doc:2017}}.

Seja $F_D(X,Y)$ a função de transformada de distância, então pode-se definir o centro da palma da mão por
\begin{equation}
    (X_{cp},Y_{cp}) = max_{(x,y)}[F_D(X,Y)],
\end{equation}
Onde $X_{cp}$ é a coordenada X do centro da palma da mão, e $Y_{cp}$ é a coordenada Y do centro da palma da mão.

A remoção da palma da mão é feita pela inserção de uma circunferência com centro coincidente com o da palma da mão e raio 85 \% maior de que a distância do centro da palma da mão à borda, sendo este raio definido empiricamente e, consequentemente, específico para a morfologia da mão utilizada como base. Processo este representado pela Figura \ref{fig:remocao_palma}.

\begin{figure}[h]
    \centering
    \caption{Remoção da palma da mão.}
    \includegraphics{deteccao_mao_2.pdf}
    \caption*{Fonte: O autor.}
    \label{fig:remocao_palma}
\end{figure}

\section{Formação de vetores característicos}

A formação de vetores é responsável por traduzir um gesto em um conjunto de vetores. A partir do resultado exibido na Figura \ref{fig:remocao_palma}, é possível observar a existência de objetos disjuntos. A partir destes objetos serão determinados quais correspondem aos dedos da mão e qual representa o pulso, para que, no fim, um gesto seja representado pelos argumentos dos vetores que conectam o centro de massa dos dedos ao centro da palma da mão, e o ângulo formado entre esses vetores e o vetor que liga o centro da palma da mão ao pulso.

\subsection{Centro de massa e vetores dos objetos disjuntos}
Como referência para a formação dos vetores, foi escolhido o centro da palma da mão como ponto de origem de todos os vetores que serão formados, que terão a outra extremidade no centro de massa de algum objeto. A identificação dos objetos separados fica a cargo da função \textit{bwlabel} \cite{bwlabel_doc:2017}. Cada objeto '$i$' encontrado tem seu centro de massa calculado por
%
\begin{equation}
    X^i_{cm} = \overline{X_i}
    \label{eq:xcm}
\end{equation}
%
e
%
\begin{equation}
    Y^i_{cm} = \overline{Y_i},
    \label{eq:ycm}
\end{equation}
%
onde $X_i[n]$ é o vetor com todas as ocorrências da coordenada $X$ onde $F_{sombra}(X,Y) = 1$ quando aplicado ao objeto '$i$'. A Equação \ref{eq:ycm} define esta mesma relação para a coordenada Y. Logo, o centro de massa do objeto $i$ é definido como
%
\begin{equation}
    Cm_i = [X^i_{cm},Y^i_{cm}].
\end{equation}

Após a computação do centro de massa de cada objeto, é definido o vetor do objeto $i$ como
%
\begin{equation}
    \overrightarrow{V}_i = (X^i_{cm}-X_{cp})\widehat{i} + (Y^i_{cm}-Y_{cp})\widehat{j}.
    \label{eq:vetor_def}
\end{equation}

\subsection{Ponto do pulso}
Dentre estes vetores ainda se encontra o vetor que conecta-se ao ponto do pulso, sendo preciso identificá-lo para que sejam calculados os ângulos formados entre os vetores que representam os dedos e o vetor que representa o pulso. 

A estratégia adota para a realização desta tarefa é a de identificar qual centro de massa é o mais externo, ou seja, qual ponto encontra-se, na média, mais distante dos demais. Para isso, determina-se a envoltória convexa de todos os subconjuntos tomados pela exclusão de um único ponto dos centros de massas de todos os objetos. Em seguida calcula-se a área de todos estes polígonos. Aquele que possuir menor área é o polígono que representa o grupo com maior agregação e o ponto de pulso será o ponto que foi excluído durante a formação deste subespaço. Para o cálculo da envoltória convexa é feita com o uso do algoritmo do tipo \textit{convex Hull} implementado pela função \textit{convhull} \cite{convhull_doc:2017}. 

Define-se o subespaço $S$ como o subespaço formado por todos os centro de massas dos objetos definidos pelas função \textit{bwlabel}, como em
%
\begin{equation}
    S = [Cm_1,Cm_2 ... Cm_i].
\end{equation}
%
Em seguida pode-se definir o subespaço $S_i$, formado pela exclusão do centro de massa $Cm_i$ do subespaço $S$, por
\begin{equation}
    S_i = S-Cm_i.
\end{equation}

Seja $Area(S_i)$ uma função que calcule a área do polígono envoltório convexo formado pelo subespaço $S_i$, o subespaço que representa os pontos correspondente aos dedos da mão é definido por
\begin{equation}
    S_{dedos} = min_{(S_i)}[Area(S_i)].
\end{equation}

Dado que o subespaço contendo todos os centros de massa é representado por $S_{mão}$, então o ponto de pulso será o ponto que resulta da operação de conjunto
\begin{equation}
    P_{pulso} = S_{mão}-S_{dedos}.
\end{equation}
Logo, o vetor que liga o centro da palma da mão ao ponto do pulso é o vetor que será usado como referência, e o ângulo formado entre este e os demais vetores, serão utilizados como uma das métricas que caracterizam um gesto.

\subsection{Cálculo de ângulos e normas}
Com o ponto do pulso determinado, resta apenas calcular a norma de cada vetor de $S_{dedos}$ e o ângulo que cada um destes faz com o vetor de referência, que é o que se coneta ao pulso. O ângulo entre dois vetores quaisquer ($\overrightarrow{V}_a$ e $\overrightarrow{V}_a$), calculado a partir da definição de produto vetorial, é dado por
\begin{equation}
    \theta = cos^{-1}\left(\frac{\overrightarrow{V}_a.\overrightarrow{V}_b}{|\overrightarrow{V}_a|.|\overrightarrow{V}_b|}\right).
\end{equation}

Desta forma, um gesto fica então definido pelo conjunto formado pelas normas dos vetores dos dedos e pelo ângulo que estes fazem com o vetor que aponta para o pulso, tal como em
\begin{equation}
    G_v = {[|\overrightarrow{V}_1|,|\overrightarrow{V}_2|,|\overrightarrow{V}_3|,...,|\overrightarrow{V}_i|, \theta_1,\theta_2,\theta_3,...,\theta_i]}.
\end{equation}

\section{Processo de Classificação do Gesto}\label{sec:class_gesto}

A etapa de classificação é aquela na qual um gesto ($G_v$) é inferido e classificado de acordo com um conjunto de referência. Neste trabalho, foram mapeados sete gestos distintos, conforme ilustrado na Figura \ref{fig:mosaico_gestos}, sendo testado três tipos diferentes de classificadores. São eles, uma rede neural, uma máquina de aprendizagem do tipo SVM, e um classificador simples, utilizando regras baseadas apenas nos ângulos presentes no \mbox{vetor $G_v$}.

\begin{figure}[h]
    \centering
    \caption[Exemplos dos gestos mapeados]{Exemplos dos gestos mapeados. Cada linha deste mosaico representa algumas amostras de um dos sete gestos mapeados.}
    \includegraphics[width=\textwidth]{exemplo_gestos.eps}
    \caption*{Fonte: O autor.}
    \label{fig:mosaico_gestos}
\end{figure}

A seguir está descrito como cada classificador foi aplicado, começando a explicação pelo classificador por regras de inferência simples, passando à rede neural e concluindo com o SVM. Estes classificadores foram escolhidos para o estudo para a formação de uma base comparativa para as soluções aplicadas. O resultado destas comparações serão devidamente apresentados no Capítulo \ref{cap:experimentos}.

\subsection{Classificador definido por limiares e regras de inferência}

Este classificador tem por base a inferência de um conjunto de limiares e regras de inferência, por meio da observação do usuário sobre o universo amostral dos dados de entrada, para a classificação de um vetor. Supondo que cada dedo da mão possui um intervalo limitado de ângulo que o define, ou seja, há uma faixa única de valores para os ângulos formados pelo vetor que o representa e o vetor que se liga ao pulso da mão, então, a partir de um conjunto de amostras, o gráfico com a função de distribuição de probabilidade (PDF, do termo, em inglês, "\textit{Probability Density Function}") destes ângulos foi levantado. A Figura \ref{fig:dist_angulos} representa uma estimativa para a PDF dos ângulos de cada dedo quando aplicadas às amostras que possuem a mão explanada (com os cinco dedos à mostra). Esta estimativa foi obtida por um universo amostral com duzentas (200) imagens da mão direita de um único indivíduo e com o auxílio da função \textit{ksdensity} \cite{ksdensity_doc:2017}.
%
\begin{figure}[h]
    \centering
    \caption[PDF estimada dos ângulos dos dedos da mão]{PDF estimada dos ângulos dos dedos da mão. Cada curva representa a função de densidade de probabilidade estimada para os ângulos que os dedos da mão fazem com o vetor do pulso na posição "mão aberta".}
    \includegraphics[width=0.9\textwidth]{distribuicao_angulos.eps}
    \caption*{Fonte: O autor.}
    \label{fig:dist_angulos}
\end{figure}

A partir destas distribuições, são definidos os intervalos de ângulos para o qual é identificado cada dedo. Auxiliar à regra de limiar há a regra de precedência, para diminuir erros de consistência como, por exemplo, a identificação de dois dedos anelares no mesmo gesto. Desta forma, a verificação da presença de um dedo se dá pelo de menor angulação ao de maior angulação, prevalecendo o que foi identificado primeiro. Assim, a função
%
\begin{equation}
    D(\theta_i) = 
    \begin{cases}
        \text{Mínimo}, & \text{se }\ \theta_i > 90\degree\ \text{ e }\ \theta_i <146\degree\\
        \text{Anelar}, & \text{se }\ \theta_i > 135 \degree\ \text{ e }\ \theta_i <168\degree\ \text{e}\ D(\theta_{i-1}) \neq\ \text{Anelar}\\ 
        \text{Médio}, & \text{se }\ \theta_i > 155\degree\ \text{ e }\ \theta_i <188\degree\ \text{e}\ D(\theta_{i-1}) \neq\ \text{Médio}\\
        \text{Indicador}, & \text{se }\ \theta_i > 170\degree\ \text{ e }\ \theta_i <215\degree\ \text{e}\ D(\theta_{i-1}) \neq\ \text{Indicador}\\
        \text{Polegar}, & \text{se }\ \theta_i > 215\degree\
    \end{cases}
\end{equation}
%
implementa um conjunto de regras para a determinação da presença de um dedo, sendo a classificação final de um gesto composta por 
%
\begin{equation}\label{eq:g_simples}
    G = [D(\theta_1),D(\theta_2),...,D(\theta_i)],
\end{equation}
%
onde $\theta_i$ corresponde ao i-ésimo ângulo do vetor $G_v$. Para que este conjunto seja eficaz, os ângulos presentes no vetor $G_v$ devem estar ordenados de forma crescente.

Aplicando esta regra ao conjunto de amostras da base de dados de referência, tem-se as matrizes de confusão à mostra na Figura \ref{fig:matriz_confusao_simples}. Nesta figura, por exemplo, para o dedo Mínimo, a taxa de falso positivo foi de 0,1\% e a de falso negativo de 40,1\%. Uma ocorrência positiva tem 73,3\% de ser corretamente identificada, já a ocorrência negativa tem 99,8\% de identificações corretas. O acerto geral do classificador, para o dedo Mínimo ficou em 80,9\%.
%
\begin{figure}[h]
    \centering
    \caption{Matriz de Confusão para o classificador definido por regras de comuns.}
    \includegraphics[width=\textwidth]{matriz_confusao_simple_class.eps}
    \caption*{Fonte: O autor.}
    \label{fig:matriz_confusao_simples}
\end{figure}

Este classificador tem, portanto, um acerto variando de oitenta a noventa porcento, dependendo do dedo avaliado.

Com um conjunto maior de regras e um ajuste mais fino do limiares de referência para cada dedo, talvez seja possível melhorar o desempenho deste classificador. Contudo, o ajuste destes parâmetros e o esforço empreendido para o alcance de um melhor resultado pode não ser um atrativo. Dessa maneira, propõe-se, agora, uma outra forma de abordar este problema, que consiste em utilizar algoritmos capazes de realizar reconhecimento de padrão. Neste sentido, o método estudado a seguir é o de classificação utilizando redes neurais.

\section{Classificador baseado em algoritmo de rede neural.}
Na seção anterior, a função $G$, definida na Equação \ref{eq:g_simples}, foi aproximada por um conjunto de regras comuns aplicada aos ângulos contidos no conjunto $G_v$, contudo o índice de acerto desta aproximação é um tanto quanto questionável. Conforme apresentado na Seção \ref{sec:RN}, uma redes neural artificial é um modelo matemático para a construção de um grafo cujo ajuste de seus parâmetros é capaz de fazer com que uma rede com multiníveis de perceptrons seja capaz de aproximar qualquer função contínua que caiba dentro de um hipercubo. Portanto, para o problema de visão computacional aqui apresentado, a rede neural artificial será utilizada para aproximar a função $G$ que aplicada sobre o conjunto de vetores $G_v$ resulta em um gesto específico. Para este fim, é definido um layout para a rede neural, que, depois de um processo de treinamento, passará a atuar como um estimador da função $G$.

\section{Layout e treinamento da rede neural.}
A construção da rede neural e seu treinamento é feito com o auxílio da ferramenta \textit{nnstart} \cite{nnstart_doc:2017}. Para o treinamento da rede neural é preciso definir um conjunto de amostras para as entradas e quais são as respostas esperadas para estas. Para a entrada são utilizadas 1400 instâncias do vetor $G_v$, resultantes das amostras do banco de dados. A Figura \ref{fig:nnstart_1} ilustra a interface inicial deste processo que é iterativo e de fácil entendimento. Nela é selecionada a opção \textit{Pattern Recognition App}, cujos parâmetros da rede neural são otimizados para reconhecimento de padrões.

\begin{figure}[h]
    \centering
    \caption{Tela 1 da ferramenta \textit{nnstart}.}
    \includegraphics{nnstart_1.png}
    \caption*{Fonte: O autor.}
    \label{fig:nnstart_1}
\end{figure}

Na Figura \ref{fig:nnstart_2}, é exibida a tela seguinte, onde pode ser visto (na região destacada) a configuração do \textit{layout} da rede neural (uma rede to tipo \textit{feedfoward} de dois níveis, um oculto e um de saída), o tipo de perceptron utilizado (\textit{softmax}, implementada na função patternnet \cite{patternnet_doc:2017} e a função de treinamento aplicada (\textit{scaled conjugated gradient backpropagation}, implementada pela função trainscg \cite{trainscg_doc:2017}. Nesta tela há também uma breve explicação, em inglês, de que tipo de uso tem o tipo de rede configurada, sendo dados alguns exemplos, como a classificação de tipo de vinho, tumores entre outros. A seta indica o botão que leva a próxima tela.

\begin{figure}[h]
    \centering
    \caption[Tela 2 da ferramenta \textit{nnstart}]{Tela 2 da ferramenta \textit{nnstart}. Configuração do \textit{layout} da rede neural.}
    \includegraphics[width=\textwidth]{nnstart_2.png}
    \caption*{Fonte: O Autor}
    \label{fig:nnstart_2}
\end{figure}
\clearpage

Após a configuração da rede, é preciso indicar quais são as variáveis de entrada e de referência para que seja feito o treinamento. Na Figura \ref{fig:nnstart_3} é possível ver a variável "fingers\underline{ }vectors" no campo de 'entradas' enquanto que "respostas" é a variável com os valores esperados. Como resposta foi adotada uma codificação do tipo \textit{one hot} em um vetor de 7 posições, cada posição representa a ocorrência de um gesto diferente sendo 1 a ocorrência positiva e 0 a negativa. As amostras estão representadas em colunas de forma que, para a entrada, tem-se uma matriz de 10x1400 e para a saída uma matriz de 7x1400.

\begin{figure}[h]
    \centering
    \caption[Tela 3 da ferramenta \textit{nnstart}]{Tela 3 da ferramenta \textit{nnstart}. Configuração das entradas e Saídas.}
    \includegraphics[width=0.8\textwidth]{nnstart_3.png}
    \caption*{Fonte: O autor.}
    \label{fig:nnstart_3}
\end{figure}

Em seguida, é configurado qual porcentagem das amostras serão utilizadas para treinamento, validação e teste, conforme a Figura \ref{fig:nnstart_4}. O grupo de treinamento é o que é passado à rede durante a etapa de ajuste de seus parâmetros, o grupo de validação é utilizado para medir a qualidade do estimador obtido, uma vez que os dados de validação não utilizados para treinamento. O treinamento termina quando o desempenho do reconhecimento do grupo de validação para de melhorar. As amostras separadas para o grupo de teste servem para o levantamento de um terceiro parâmetro a cerca da qualidade do estimador obtido, uma vez que elas não são usadas para o treinamento e nem para a validação, então elas são capazes de fornecer uma medida da generalização alcançada. Sem este último grupo, há uma chance maior de que o estimador tenha se transformado em um banco de memória.

\begin{figure}[h]
    \centering
    \caption[Tela 4 da ferramenta \textit{nnstart}]{Tela 4 da ferramenta \textit{nnstart}.Configuração do grupo de treino, validação e teste. Do total de amostras, 70\% é utilizado para treinamento, 15\% para validação e 15\% para teste.}
    \includegraphics[width=.7\textwidth]{nnstart_4.png}
    \caption*{Fonte: O autor.}
    \label{fig:nnstart_4}
\end{figure}

A Figura \ref{fig:net_layer} mostra a estrutura da rede neural, onde foram definidas duas camadas, uma oculta e outra de saída, sendo a primeira com 10 perceptrons e a última com 7. 

\begin{figure}[h]
    \centering
    \caption{Estrutura da rede neural.}
    \includegraphics[width=0.7\textwidth]{net_layer.png}
    \caption*{Fonte: O autor.}
    \label{fig:net_layer}
\end{figure}

Ao fim da configuração da rede neural, tem-se o resultado que se observa na Figura \ref{fig:confusion_RN}, onde as classes 1 a 7 representam os gestos $G_1$ a $G_7$, respectivamente. Nesta figura, é notável a melhora no desempenho em relação ao classificador feito com regras comuns. Para o pior caso, para o desempenho geral, tem-se uma taxa de verdadeiro positivo de 91\% para o $G_7$, sendo a acerto geral de 97,7\%. Em comparação ao desempenho do caso anterior, houve uma melhora média de 14,5\%, isso mostra que a aproximação feita pela rede neural é consideravelmente mais eficaz de que o caso em que foi aplicada regras comuns.
%
\begin{figure}[h]
    \centering
    \caption{Matriz de confusão gerada ao fim do treinamento da rede neural com a ferramenta \textit{nnstart}.}
    \includegraphics[width=\textwidth]{confusion_rn.png}
    \caption*{Fonte: O autor.}
    \label{fig:confusion_RN}
\end{figure}

Como mostrado nesta seção, um estimador baseado em uma máquina de aprendizagem, como a rede neural, é capaz de inferir uma função capaz aprender, estimar, uma função que leve é capaz de aferir um gesto a partir do vetor  $G_v$. Na seção seguinte, será avaliado o mesmo problema com uma abordagem com solução utilizando SVMs.

\section{Classificador utilizando máquina de vetor de suporte}
Uma máquina de vetor de suporte, assim como uma rede neural, é capaz de mapear a função que conecta o conjunto de entradas em suas respectivas saídas, atuando como um estimador. Para tanto este também passa por um processo de treinamento, que aqui é feito com o auxílio da toolbox do matlab \textit{classificationLearner} \cite{classificationlearner_doc:2017}, onde será obtido um estimador para a função $G$.

A interface inicial da ferramenta em questão, é apresentada na Figura \ref{fig:svm_train1}. Nela é possível selecionar qual o conjunto de dados que serão utilizados, tanto as entradas quanto as respostas, bem como qual será o método utilizado para a separação do grupo de treinamento e grupo de validação.

\begin{figure}[h]
    \centering
    \caption{Interface inicial do classificationLeaner.}
    \includegraphics[width=\textwidth]{svm_train_1.PNG}
    \caption*{Fonte: O autor.}
    \label{fig:svm_train1}
\end{figure}

Dentre os métodos disponíveis (a saber, \textit{Cross Validation}, \textit{Hold Out}, \textit{No Validation} ), foi adotado o de validação cruzada (\textit{Cross Validation}), onde os dados são divididos em 'n' grupos iguais e, para cada grupo de dados, é feito o treinamento de uma máquina SVM. Após o treinamento dentro do seu segmento, as máquinas são validadas com os dados dos demais grupos, sendo preservada a que obteve o maior grau de generalização.

Na tela seguinte, há um conjunto de métodos diferentes de configurações para a máquina de vetor de suporte, sendo possível testar todas elas e escolher a que melhor se adéqua ao problema. Como mostra o resultado na Figura \ref{fig:svm_train2} a configuração que obteve melhor desempenho foi a SVM do tipo "Subspace KNN" com um acerto total de 99,3\%. Superando até mesmo o caso estudado com a classificação feita por rede neural. 

\begin{figure}[h]
    \centering
    \caption{Desempenho dos diferentes tipos de SVM.}
    \includegraphics[width=\textwidth]{svm_train_2.PNG}
    \caption*{Fonte: O autor.}
    \label{fig:svm_train2}
\end{figure}

Nesta mesma interface é possível visualizar a curva característica de operação do receptor (ROC, do termo em inglês \textit{Receiver Operating Characteristic}), a matriz de confusão, e o gráfico que relaciona, duas as duas, as variáveis e classificações obtidas. A Figura \ref{fig:confuion_svm} mostra a matriz de confusão exibida por este programa, sendo os gestos G1 a G7 representados, nas linhas, de forma crescente de cima para baixo, ou seja a primeira linha representando o gesto G1 e a última linha o gesto G7, enquanto que nas colunas, a primeira coluna da esquerda representa o gesto G1 enquanto que a última coluna, à direita, representa o gesto G7. 

\begin{figure}[h]
    \centering
    \caption{Matriz de confusão para o classificador baseado em uma SVM.}
    \includegraphics{matriz_confu_svm.png}
    \caption*{Fonte: O autor.}
    \label{fig:confuion_svm}
\end{figure}
\clearpage
Portanto, encerra-se este capítulo tendo sido exibido todo o procedimento realizado para a identificação de um gesto, onde, a partir de uma imagem, é extraído a região que corresponde à mão, pela detecção da cor da pele humana. Com a mão delimitada, são gerados vetores que resumem as principais características do gesto proferido, e estes vetores são então apresentados a um classificador. Dentre os classificadores testados, temos um de uso de regras comuns, que é de implementação mais simples e é dependente da capacidade de observação e abstração daquele que define as regras de inferência. Tendo este primeiro classificador obtido um desempenho entre 80\% e 90\%, dependendo do dedo avaliado, sendo capaz de identificar os dedos individualmente, e não necessariamente um gesto específico. E, por fim, os classificadores desenvolvidos com o uso de algoritmos de aprendizagem, que foram a rede neural artificial,  com um desempenho de 97,7\% de acerto, e o classificador com uma SVM, com um desempenho de 99,3\%. Estes dois últimos são capazes de identificar um dos sete gestos da base de dados, sendo necessário o retreinamento, com novos dados, caso queira-se adicionar outros gestos.